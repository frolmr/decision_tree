# Decision tree app

This is Sinatra application build as test task

Web app lives [[here]](https://guarded-savannah-81188.herokuapp.com/)

To check the app:
- pull repo
- move to dir
- start app: `ruby app.rb`

To run specs run `rspec` inside app dir
