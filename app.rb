# frozen_string_literal: true

require 'sinatra'
require 'sinatra/activerecord'
require './environments'
require './app/controllers/actions_controller'
require './app/controllers/tree_controller'

set :views, proc { File.join(root, 'app/views') } # rubocop:disable Lint/AmbiguousBlockAssociation
set :show_exceptions, false
set :raise_errors, false

use ActionsController
use TreeController

get '/' do
  erb :index
end

error do
  { error: 500 }.to_json
end
