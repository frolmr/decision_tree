# frozen_string_literal: true

class NodeBuilder
  @id = nil

  def initialize(tree: {}, context: {})
    @tree = tree
    @context = context
  end

  # TODO: optimize algo (stack too deep problem)
  # TODO: fix case when many default values
  def create
    key = @context.keys.first
    value = @context[key]
    if no_root_key_yet(key, value)
      build_inital_strcture(key, value)
    elsif no_more_elemnts
      return [id]
    elsif key_and_value_already_present(key, value)
      add_top_level_default(key)
    elsif key_already_present(key)
      add_key_to_values(key, value)
    else add_default
    end
    @tree
  end

  private

  #-----------------------CONDITIONS-------------------------
  def no_root_key_yet(key, value)
    @tree[key] != value && @tree[:key].nil?
  end

  def no_more_elemnts
    @tree.empty? && @context.empty?
  end

  def key_and_value_already_present(key, value)
    @tree[:key] == key && @tree[:values].include?(value)
  end

  def key_already_present(key)
    @tree[:key] == key
  end

  #----------------------Modificatons-------------------------
  def build_inital_strcture(key, value)
    @tree[:key] = key
    if @tree[:values].nil?
      @tree[:values] = { value.to_sym => NodeBuilder.new(context: remaining_context(key)).create }
    end
    @tree[:default] = []
  end

  def add_top_level_default(key)
    @tree[:values][:default] =
      NodeBuilder.new(context: remaining_context(key)).create
  end

  def add_key_to_values(key, value)
    @tree[:values][value.to_sym] =
      NodeBuilder.new(context: remaining_context(key)).create
  end

  def add_default
    @tree[:default] = NodeBuilder.new(context: @context).create
  end

  #-----------------------Helpers------------------------------
  def remaining_context(key)
    @context.reject { |k, _| k == key }
  end

  def id
    @@id
  end
end
