# frozen_string_literal: true

require_relative 'node_builder'

class TreeBuilder
  def initialize(actions)
    @actions = actions
  end

  def create
    @actions.each_with_object({}) do |action, acc|
      NodeBuilder.class_variable_set(:@@id, action[:id])
      NodeBuilder.new(tree: acc, context: action[:properties]).create
    end
  end
end
