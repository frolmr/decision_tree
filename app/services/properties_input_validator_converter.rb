# frozen_string_literal: true

class PropertiesInputValidatorConverter
  OUTER_CURLY_BRACES_REGEX = /(^\{|\}$)/
  INNER_CURLY_BRACES_REGEX = /(\{|\})/

  def initialize(input)
    @input = input[:properties]
  end

  def validate_and_convert
    [validate_input, convert_input(validate_input)]
  end

  private

  def validate_input
    # NOTE check on nested hashes
    (@input.gsub(OUTER_CURLY_BRACES_REGEX, '') =~ INNER_CURLY_BRACES_REGEX).nil?
  end

  def convert_input(is_valid)
    return unless is_valid
    @input.gsub(/\s+/, '').split(',').each_with_object({}) do |el, acc|
      key, value = el.split(':')
      acc[key] = value
    end
  end
end
