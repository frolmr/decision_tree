# frozen_string_literal: true

class ActionsPrioritization
  def initialize(actions)
    @actions = actions
  end

  def prioritize
    properties = fetch_properties

    keys_frequencies = get_keys_frequencies_from(properties)
    keys_weights = get_keys_weights(keys_frequencies, properties)

    weighted_sorted_props = weight_props(properties, keys_weights)
                            .yield_self(&method(:sort_by_key))

    root_node = get_root_node(weighted_sorted_props, keys_weights)

    prioritize_by(root_node, weighted_sorted_props)
      .yield_self(&method(:build_result))
  end

  private

  def fetch_properties
    @actions.map { |action| action[:properties] }
  end

  def get_keys_frequencies_from(properties)
    properties.map(&:keys)
              .flatten
              .group_by { |el| el }
              .transform_values(&:size)
  end

  def get_keys_weights(keys, properties)
    keys.each_with_object({}) do |(k, v), res|
      acc = v
      properties.each do |el|
        acc += el.size if el.key? k
      end
      res[k] = acc
    end
  end

  def weight_props(props, keys_weights)
    props.map do |prop|
      sum = prop.keys.inject(0) { |acc, el| acc + keys_weights[el] }
      { sum => prop }
    end
  end

  def get_root_node(props, keys_weights)
    props.first.values.first
         .map { |k, v| { keys_weights[k] => { k => v } } }
         .yield_self(&method(:sort_by_key))
         .map { |k, _| k.values.first }
         .reduce({}, :merge)
  end

  def prioritize_by(mask, props)
    props.flat_map(&:values).map do |prop|
      mask.keys.each_with_object({}) do |key, acc|
        if prop[key]
          new_val = prop[key]
          acc[key] = new_val
        end
        acc.merge!(prop)
      end
    end
  end

  def build_result(data)
    data.map do |el|
      id = @actions.select { |action| action[:properties] == el }.first[:id]
      { id: id, properties: el }
    end
  end

  def sort_by_key(data)
    data.sort { |a, b| b.keys.first <=> a.keys.first }
  end
end
