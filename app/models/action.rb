# frozen_string_literal: true

class Action < ActiveRecord::Base
  validates :properties, presence: true, uniqueness: true, length: { maximum: 10 }
end
