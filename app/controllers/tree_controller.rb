# frozen_string_literal: true

require_relative '../services/actions_prioritization'
require_relative '../services/tree_builder'

class TreeController < Sinatra::Base
  configure do
    set :views, 'app/views'
  end

  get '/build_tree' do
    if Action.any?
      actions = Action.all
      nodes = ActionsPrioritization.new(actions).prioritize
      @tree = TreeBuilder.new(nodes).create
      erb :"tree/show"
    else
      'No actions yet'
    end
  end
end
