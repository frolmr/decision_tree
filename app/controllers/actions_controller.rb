# frozen_string_literal: true

require_relative '../models/action'
require_relative '../services/properties_input_validator_converter'

class ActionsController < Sinatra::Base
  configure do
    set :views, 'app/views'
  end

  get '/actions' do
    @actions = Action.all
    erb :"actions/index"
  end

  get '/actions/new' do
    @action = Action.new
    erb :"actions/new"
  end

  post '/actions' do
    is_valid, properties = PropertiesInputValidatorConverter.new(params[:action])
                                                            .validate_and_convert
    @action = Action.new(properties: properties)
    if !is_valid
      { error: 400 }.to_json
    elsif is_valid && @action.save
      redirect 'actions'
    else
      erb :"actions/new"
    end
  end
end
