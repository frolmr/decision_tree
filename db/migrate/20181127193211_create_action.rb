class CreateAction < ActiveRecord::Migration[5.2]
  enable_extension 'hstore' unless extension_enabled?('hstore')

  def change
    create_table :actions do |t|
      t.hstore 'properties'
    end
  end
end
