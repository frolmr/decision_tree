# frozen_string_literal: true

RSpec.shared_context 'with reference actions' do
  let(:action_11) do
    { id: 11, properties: { color: :green, location: :unknown } }
  end

  let(:action_11_inv) do
    { id: 11, properties: { location: :unknown, color: :green } }
  end

  let(:action_12) do
    { id: 12, properties: { color: :red, real: :no } }
  end

  let(:action_12_inv) do
    { id: 12, properties: { real: :no, color: :red } }
  end

  let(:action_13) do
    { id: 13, properties: { location: :moscow } }
  end

  let(:action_14) do
    { id: 14, properties: { location: :unknown, type: :tool } }
  end

  let(:action_15) do
    { id: 15, properties: { location: :unknown } }
  end

  let(:action_16) do
    { id: 16, properties: { type: :human, name: :Vasya } }
  end

  let(:action_17) do
    { id: 17, properties: { real: :no, type: :dwarf, weapon: :axe } }
  end
end
