# frozen_string_literal: true

RSpec.shared_context 'with reference trees' do
  let(:example_tree) do
    {
      key: :color,
      values: {
        green: {
          key: :location,
          values: {
            unknown: [11]
          },
          default: []
        },
        red: {
          key: :real,
          values: {
            no: [12]
          },
          default: []
        }
      },
      default: {
        key: :location,
        values: {
          moscow: [13]
        },
        default: []
      }
    }
  end

  let(:tree_1) do
    {
      key: :location,
      values: {
        unknown: {
          key: :type,
          values: {
            tool: [14]
          },
          default: []
        },
        moscow: [13]
      },
      default: {
        key: :color,
        values: {
          red: {
            key: :real,
            values: {
              no: [12]
            },
            default: []
          }
        },
        default: []
      }
    }
  end

  let(:tree_2) do
    {
      key: :location,
      values: {
        unknown: {
          key: :type,
          values: {
            tool: [14]
          },
          default: []
        },
        default: [15]
      },
      default: {
        key: :color,
        values: {
          red: {
            key: :real,
            values: {
              no: [12]
            },
            default: []
          }
        },
        default: []
      }
    }
  end
end
