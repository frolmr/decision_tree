# frozen_string_literal: true

require_relative '../../app/services/actions_prioritization'
require_relative '../shared_context_reference_actions'
require_relative '../shared_examples_correct_working_module'

RSpec.describe ActionsPrioritization do
  include_context 'with reference actions'

  subject { described_class.new(actions).prioritize }

  context 'when actions 11, 12, 13' do
    let(:actions) { [action_11_inv, action_13, action_12] }

    let(:result) { [action_11, action_12, action_13] }

    it_should_behave_like 'a module returning correct result'
  end

  context 'when actions 15, 12, 14' do
    let(:actions) { [action_15, action_12, action_14] }

    let(:result) { [action_14, action_12, action_15] }

    it_should_behave_like 'a module returning correct result'
  end

  context 'when actions 17, 12_inv, 11' do
    let(:actions) { [action_17, action_12_inv, action_11] }

    let(:result) do
      [
        { id: 17, properties: { real: :no, type: :dwarf, weapon: :axe } },
        { id: 12, properties: { color: :red, real: :no } },
        { id: 11, properties: { color: :green, location: :unknown } }
      ]
    end

    it_should_behave_like 'a module returning correct result'
  end

  context 'when actions 15, 11_inv, 13' do
    let(:actions) { [action_15, action_11_inv, action_13] }

    let(:result) do
      [
        { id: 11, properties: { color: :green, location: :unknown } },
        { id: 15, properties: { location: :unknown } },
        { id: 13, properties: { location: :moscow } }
      ]
    end

    it_should_behave_like 'a module returning correct result'
  end

  context 'when custom actions' do
    let(:actions) do
      [
        { id: 1, properties: { color: :green } },
        { id: 2, properties: { real: :no, type: :elf, weapon: :bow } },
        { id: 3, properties: { real: :no, color: :green, name: :shrek } }
      ]
    end

    let(:result) do
      [
        {id: 3, properties: { color: :green, name: :shrek, real: :no}},
        {id: 2, properties: { real: :no, type: :elf, weapon: :bow}},
        {id: 1, properties: { color: :green}}
      ]
    end

    it_should_behave_like 'a module returning correct result'
  end
end
