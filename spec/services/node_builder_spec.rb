# frozen_string_literal: true

require_relative '../../app/services/node_builder'
require_relative '../shared_examples_correct_working_module'

RSpec.describe NodeBuilder do
  subject { described_class.new(tree: tree, context: context).create }

  before do
    described_class.class_variable_set(:@@id, 88)
  end

  context 'when empty tree and full context' do
    let(:tree) do
      {}
    end

    let(:context) do
      { location: :unknown, color: :red }
    end

    let(:result) do
      {
        key: :location,
        values: {
          unknown: {
            key: :color,
            values: {
              red: [88]
            },
            default: []
          }
        },
        default: []
      }
    end

    it_should_behave_like 'a module returning correct result'
  end

  context 'when tree and context prent' do
    let(:tree) do
      {
        key: :color,
        values: {
          green: [77]
        },
        default: []
      }
    end

    let(:context) do
      { color: :red, real: :no }
    end

    let(:result) do
      {
        key: :color,
        values: {
          green: [77],
          red: {
            key: :real,
            values: {
              no: [88]
            },
            default: []
          }
        },
        default: []
      }
    end

    it_should_behave_like 'a module returning correct result'
  end
end
