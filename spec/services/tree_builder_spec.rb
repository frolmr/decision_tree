# frozen_string_literal: true

require_relative '../../app/services/tree_builder'
require_relative '../shared_context_reference_trees'
require_relative '../shared_context_reference_actions'
require_relative '../shared_examples_correct_working_module'

RSpec.describe TreeBuilder do
  include_context 'with reference trees'
  include_context 'with reference actions'

  subject { described_class.new(actions).create }

  context 'with example set' do
    let(:actions) { [action_11, action_12, action_13] }

    let(:result) { example_tree }

    it_should_behave_like 'a module returning correct result'
  end

  context 'with example 1' do
    let(:actions) { [action_14, action_13, action_12] }

    let(:result) { tree_1 }

    it_should_behave_like 'a module returning correct result'
  end

  context 'with example 2' do
    let(:actions) { [action_14, action_15, action_12] }

    let(:result) { tree_2 }

    it_should_behave_like 'a module returning correct result'
  end
end
