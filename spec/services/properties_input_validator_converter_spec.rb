# frozen_string_literal: true

require_relative '../../app/services/properties_input_validator_converter'

RSpec.describe PropertiesInputValidatorConverter do
  subject { described_class.new(input).validate_and_convert }

  context 'when input contain nested hashes' do
    let!(:input) do
      {
        'properties': "name: vasya, type: {dwarf: yes, elf: no}"
      }
    end

    it 'returns is_valid as false' do
      expect(subject).to eq([false, nil])
    end
  end

  context 'when input does not contain nested hashes' do
    let!(:input) do
      {
        'properties': "name: vasya, type: dwarf, elf: no}"
      }
    end

    it 'returns is_valid as false' do
      expect(subject).to eq([true, {"elf"=>"no}", "name"=>"vasya", "type"=>"dwarf"}])
    end
  end
end

