# frozen_string_literal: true

RSpec.shared_examples 'a module returning correct result' do
  it 'returns correct result' do
    expect(subject).to eq result
  end
end
